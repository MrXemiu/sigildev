﻿using Emgu.CV;
using Emgu.CV.Structure;
using FiveKeys.net.Sigil.CV;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using Tests.Properties;

namespace Tests
{
  [TestFixture]
  public class MatchServiceTests
  {


    protected static readonly ILog log = LogManager.GetLogger(typeof(MatchServiceTests));

    [SetUp]
    public void Setup()
    {
      XmlConfigurator.Configure();
    }

    [Test]
    [Ignore]
    public void IndexSelectionTest()
    {
      log.InfoFormat("{2}{0}Begin IndexSelectionTest{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
      var intList = new List<int>();
      for(var i = 100; i < 120; i++)
        intList.Add(i);

      var evenIndexes = intList.Select((num, index) => new { num, index }).Where(n => n.num % 2 == 0).Select(i => i.index);

      foreach(var integer in evenIndexes)
        Console.Write("{0}, ", integer);

      log.InfoFormat("{2}{0}End IndexSelectionTest{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
    }

    [Test]
    public void MatchService_ImageContainsImage_ReturnsFalseWhenImageIsNotInScene()
    {
      log.InfoFormat("{2}{0}Begin ReturnsFalseWhenImageIsNotInScene{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
      var matchService = new MatchService(log);
      //TODO get an image that isn't in the scene
      var image = new Image<Gray, byte>(Resources.box);
      var scene = new Image<Gray, byte>(Resources.no_box_in_scence);
      var result = matchService.SceneContainsImage(scene, image);
      Assert.IsFalse(result);
      log.InfoFormat("{2}{0}End ReturnsFalseWhenImageIsNotInScene{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
    }

    [Test]
    public void MatchService_ImageContainsImage_ReturnsTrueWhenImageIsInScene()
    {
      log.InfoFormat("{2}{0}Begin ReturnsTrueWhenImageIsInScene{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
      var matchService = new MatchService(log);

      var image = new Image<Gray, byte>(Resources.box);
      var scene = new Image<Gray, byte>(Resources.box_in_scene);
      var result = matchService.SceneContainsImage(scene, image);
      Assert.IsTrue(result);
      log.InfoFormat("{2}{0}End ReturnsTrueWhenImageIsInScene{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
    }

    [Test]
    public void MatchService_ImageContainsImage_Returns_100Percent_MatchRatio_WhenModelAndSceneAreTheSame()
    {
      log.InfoFormat("{2}{0}Begin 100Percent_MatchRatio_WhenModelAndSceneAreTheSame{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
      var matchService = new MatchService(log);

      var image = new Image<Gray, byte>(Resources.box);
      var scene = new Image<Gray, byte>(Resources.box);
      var result = matchService.SceneContainsImage(scene, image);
      Assert.IsTrue(result);
      log.InfoFormat("{2}{0}End 100Percent_MatchRatio_WhenModelAndSceneAreTheSame{1}{2}", "*-----   ", "   -----*", Environment.NewLine);
    }
  }
}