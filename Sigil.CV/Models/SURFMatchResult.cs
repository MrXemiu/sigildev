﻿using System.Linq;
using Emgu.CV;
using Emgu.CV.Features2D;
using Emgu.CV.Util;

namespace FiveKeys.net.Sigil.CV.Models
{
  public struct SURFMatchResult
  {
    private Features2DTracker<float>.MatchedImageFeature[] _matchedFeatures;
    public long? MatchTime { get; set; }
    public VectorOfKeyPoint ModelKeyPoints { get; set; }
    public VectorOfKeyPoint ObservedKeyPoints { get; set; }

    public Features2DTracker<float>.MatchedImageFeature[] MatchedFeatures
    {
      get { return _matchedFeatures ?? new Features2DTracker<float>.MatchedImageFeature[0]; }
      set { _matchedFeatures = value; }
    }

    public double MatchRatio { get { return (double)MatchedFeatures.Length/ModelKeyPoints.Size; } }
    public Matrix<int> ObservedIndices { get; set; }
    public Matrix<byte> ObservedMask { get; set; }
    public HomographyMatrix Homography { get; set; }
    public bool IsMatch { get { return Homography != null; } }
  }
}
