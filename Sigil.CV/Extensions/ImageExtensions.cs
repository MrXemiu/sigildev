﻿using Emgu.CV;
using Emgu.CV.Structure;

#if !IOS
#endif

namespace FiveKeys.net.Sigil.CV.Extensions
{
  public static class ImageExtensions
  {
    public static bool Contains(this Image<Gray, byte> image, Image<Gray,byte> other, IMatchService service)
    {
      return service.SceneContainsImage(image, other);
    }
  }
}
