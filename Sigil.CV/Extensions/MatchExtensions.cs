﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.Util;

namespace FiveKeys.net.Sigil.CV.Extensions
{
  public static class MatchExtensions
  {
    public static string PrettyPrint(this VectorOfKeyPoint vector)
    {
      return string.Format("{{{0}}}", string.Join(", ", vector.ToArray()));
    }

    public static string PrettyPrint(this HomographyMatrix matrix)
    {
      var sb = new StringBuilder("[");
      for(var i = 0; i < matrix.Height; ++i)
      {
        sb.Append("[");
        for(var j = 0; j < matrix.Width; ++j)
        {
          sb.AppendFormat("{0}{1}", matrix.Data[i, j], j + 1 < matrix.Width ? ", " : "");
        }
        sb.AppendFormat("]{0}", i + 1 < matrix.Height ? ", " + Environment.NewLine : "");
      }
      sb.Append("]");

      return sb.ToString();
    }
  }
}
