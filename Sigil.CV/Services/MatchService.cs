﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Features2D;
using Emgu.CV.GPU;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using FiveKeys.net.Sigil.CV.Extensions;
using FiveKeys.net.Sigil.CV.Models;
using log4net;

namespace FiveKeys.net.Sigil.CV
{
  public interface IMatchService
  {
    IEnumerable<int> IndexOfMatch(Image<Gray, byte> scene, List<Image<Gray, byte>> models);

    bool SceneContainsImage(Image<Gray, byte> sample, Image<Gray, byte> model);

    bool SceneContainsModel(Image<Gray, byte> scene, GpuMat<float> gpuModelKeyPoints, GpuMat<float> gpuModelDescriptors);

    SURFMatchResult MatchImageInScene(Image<Gray, byte> scene, Image<Gray, byte> model, int neighborhood = 2, double uniquenessThreshold = 0.8);

    SURFMatchResult MatchModelInScene(Image<Gray, byte> scene, GpuMat<float> gpuModelKeyPoints, GpuMat<float> gpuModelDescriptors, int neighborhood = 2, double uniquenessThreshold = 0.8);
  }

  public class MatchService : IMatchService
  {
    private readonly ILog _log;
    private readonly GpuSURFDetector _surfGPU;
    private readonly SURFDetector _surfCPU;
    private const double hessian_threshold = 500;
    private const bool surf_extended_flags = false;
    private const float features_ratio = 0.01f;
    private const int reprojection_error_threshold = 2;
    private const double scale_increment = 1.5;
    private const int rotation_bins = 20;

    public MatchService(ILog log)
    {
      _log = log;

      //TODO learn SURF @ http://docs.opencv.org/modules/nonfree/doc/feature_detection.html
      _surfCPU = new SURFDetector(hessian_threshold, surf_extended_flags);
      _surfGPU = new GpuSURFDetector(_surfCPU.SURFParams, features_ratio);
    }

    public IEnumerable<int> IndexOfMatch(Image<Gray, byte> scene, List<Image<Gray, byte>> models)
    {
      return models.Select((image, index) => new { image, index }).Where(arg => scene.Contains(arg.image, this)).Select(i => i.index);
    }

    public bool SceneContainsImage(Image<Gray, byte> scene, Image<Gray, byte> model)
    {
      var result = MatchImageInScene(scene, model);
      return SURFMatchResultIsMatch(result);
    }

    public bool SceneContainsModel(Image<Gray, byte> scene, GpuMat<float> gpuModelKeyPoints, GpuMat<float> gpuModelDescriptors)
    {
      var result = MatchModelInScene(scene, gpuModelKeyPoints, gpuModelDescriptors);
      return SURFMatchResultIsMatch(result);
    }

    public SURFMatchResult MatchImageInScene(Image<Gray, byte> scene, Image<Gray, byte> model, int neighborhood = 2, double uniquenessThreshold = 0.8)
    {
      //extract features from the object image
      using(var gpuModelImage = new GpuImage<Gray, byte>(model))
      using(var gpuModelKeyPoints = _surfGPU.DetectKeyPointsRaw(gpuModelImage, null))
      using(var gpuModelDescriptors = _surfGPU.ComputeDescriptorsRaw(gpuModelImage, null, gpuModelKeyPoints))
      {
        return MatchModelInScene(scene, gpuModelKeyPoints, gpuModelDescriptors, neighborhood, uniquenessThreshold);
      }
    }

    public SURFMatchResult MatchModelInScene(Image<Gray, byte> scene, GpuMat<float> gpuModelKeyPoints, GpuMat<float> gpuModelDescriptors, int neighborhood = 2, double uniquenessThreshold = 0.9)
    {
      var result = new SURFMatchResult
      {
        ModelKeyPoints = new VectorOfKeyPoint(),
        ObservedKeyPoints = new VectorOfKeyPoint()
      };


      using(var matcher = new GpuBruteForceMatcher<float>(DistanceType.L2))
      {
        _surfGPU.DownloadKeypoints(gpuModelKeyPoints, result.ModelKeyPoints);

        var timer = Stopwatch.StartNew();

        // extract features from the observed image
        using(var gpuObservedImage = new GpuImage<Gray, byte>(scene))
        using(var gpuObservedKeyPoints = _surfGPU.DetectKeyPointsRaw(gpuObservedImage, null))
        using(var gpuObservedDescriptors = _surfGPU.ComputeDescriptorsRaw(gpuObservedImage, null, gpuObservedKeyPoints))
        using(var gpuMatchIndices = new GpuMat<int>(gpuObservedDescriptors.Size.Height, neighborhood, 1, true))
        using(var gpuMatchDist = new GpuMat<float>(gpuObservedDescriptors.Size.Height, neighborhood, 1, true))
        using(var gpuMask = new GpuMat<byte>(gpuMatchIndices.Size.Height, 1, 1))
        using(var stream = new Stream())
        {
          matcher.KnnMatchSingle(gpuObservedDescriptors, gpuModelDescriptors, gpuMatchIndices, gpuMatchDist, neighborhood, null, stream);
          result.ObservedIndices = new Matrix<int>(gpuMatchIndices.Size);
          result.ObservedMask = new Matrix<byte>(gpuMask.Size);

          //gpu implementation of voteForUniquess
          using(var col0 = gpuMatchDist.Col(0))
          using(var col1 = gpuMatchDist.Col(1))
          {
            GpuInvoke.Multiply(col1, new MCvScalar(uniquenessThreshold), col1, stream);
            GpuInvoke.Compare(col0, col1, gpuMask, CMP_TYPE.CV_CMP_LE, stream);
          }

          _surfGPU.DownloadKeypoints(gpuObservedKeyPoints, result.ObservedKeyPoints);

          //wait for the stream to complete its tasks
          //We can perform some other CPU intesive stuffs here while we are waiting for the stream to complete.
          stream.WaitForCompletion();

          gpuMask.Download(result.ObservedMask);
          gpuMatchIndices.Download(result.ObservedIndices);

          if(GpuInvoke.CountNonZero(gpuMask) >= 4)
          {
            var modelFeatures = ImageFeature<float>.ConvertFromRaw(result.ModelKeyPoints, gpuModelDescriptors.ToMatrix());
            var tracker = new Features2DTracker<float>(modelFeatures);

            var observedFeatures = ImageFeature<float>.ConvertFromRaw(result.ObservedKeyPoints, gpuObservedDescriptors.ToMatrix());

            //result.Homography = tracker.Detect(observedFeatures, uniquenessThreshold);
            var matchFeatures = tracker.MatchFeature(observedFeatures, 2);

            var matchedGoodFeatures = Features2DTracker<float>.VoteForSizeAndOrientation(matchFeatures, scale_increment, rotation_bins);
            
            if(matchedGoodFeatures.Length >= 4)
            {
              result.Homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(result.ModelKeyPoints, result.ObservedKeyPoints, result.ObservedIndices, result.ObservedMask, reprojection_error_threshold);

              if (result.Homography != null)
              {
                var matchedFeaturePoints = matchedGoodFeatures.Select(f => f.ObservedFeature.KeyPoint.Point).ToArray();

                //apply the homography matrix to the matched features from the observed image.  This should result in an image that matches the model even more closely
                result.Homography.ProjectPoints(matchedFeaturePoints);

                //compute the homography matrix between the model and the transformed matched feature set
                var newHomography = tracker.Detect(matchedGoodFeatures.Select(f => f.ObservedFeature).ToArray(), uniquenessThreshold);

                _log.DebugFormat("Features2DToolBox Homography points: {1}{0}{1}", result.Homography.PrettyPrint(), Environment.NewLine);
                _log.DebugFormat("Features2DTracker Homography points: {1}{0}{1}", newHomography == null ? "NULL" : newHomography.PrettyPrint(), Environment.NewLine);

                if (newHomography != null)
                {
                  result.MatchedFeatures = matchedGoodFeatures;
                }
                else
                  result.Homography = null;
              }
            }
          }

          timer.Stop();
          result.MatchTime = timer.ElapsedMilliseconds;
        }
      }
      return result;
    }

    private bool SURFMatchResultIsMatch(SURFMatchResult matchResult)
    {
      _log.DebugFormat("Feature match time:  {0}", matchResult.MatchTime);
      _log.DebugFormat("Model key points: {0}", matchResult.ModelKeyPoints.Size);
      _log.DebugFormat("Observed key points: {0}", matchResult.ObservedKeyPoints.Size);
      _log.DebugFormat("Match ratio: {0:P}", matchResult.MatchRatio);
      _log.InfoFormat("IsMatch:  {0}", matchResult.IsMatch);
      return matchResult.IsMatch;
    }
  }
}